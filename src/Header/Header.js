import React, { Component } from 'react';

class Header extends Component {
    render() {
        return (
        <header>
            <div className="top-row">
                <button className="header-deposit-btn">Deposit</button>
                <ul>
                    <li><a href="#">Transfer</a></li>
                    <li><a href="#">Balance ($599.99)</a></li>
                    <li><a href="#">Open bets ($300)</a></li>
                    <li><a href="#">(0) Croatia</a></li>
                    <li><a href="#">My account</a></li>
                </ul>
            </div>
            <div className="mid-row">
                <div className="logo"></div>
                <ul>
                    <li><a href="#">Sports</a></li>
                    <li><a href="#">Live Betting</a></li>
                    <li><a href="#">Casino</a></li>
                    <li><a href="#">Games</a></li>
                    <li><a href="#">Live Casino</a></li>
                </ul>
            </div>
        </header>
        );
    }
}

export default Header;